﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiemDanhSinhVien_BE.Helpers
{
    public class DateTimeHelpers
    {
        public static string toDateString(string date , string format = "dd/MM/yyyy")
        {
            return DateTime.Parse(date).ToString(format);
        }
    }
}
