﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiemDanhSinhVien_BE.Helpers
{
    public class PasswordHelpers
    {
        public static string CreatePassword(string password)
        {
            string salt = BCrypt.Net.BCrypt.GenerateSalt();
            string hashed = BCrypt.Net.BCrypt.HashPassword(password, salt);
            return $"{hashed}";
        }
    }
}
