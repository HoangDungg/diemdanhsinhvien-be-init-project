﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiemDanhSinhVien_BE.Helpers
{
    public class RandomIDHelpers
    {
        private static int GetYear()
        {
            var y = DateTime.Now.ToString("yy");
            int i = int.Parse(y);
            return i;
        }

        private static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);

        }

        public static int RandomId()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(GetYear());
            builder.Append(RandomNumber(0, 999999));
            int id = int.Parse(builder.ToString());
            return id;
        }
    }
}
