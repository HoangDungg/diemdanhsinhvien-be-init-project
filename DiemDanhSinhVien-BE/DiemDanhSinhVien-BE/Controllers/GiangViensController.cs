﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DiemDanhSinhVien_BE.Model;
using System.Net;
using DiemDanhSinhVien_BE.Helpers;
using DiemDanhSinhVien_BE.DTO;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using DiemDanhSinhVien_BE.Model.Binding.GiangVien;

namespace DiemDanhSinhVien_BE.Controllers
{
    [Route("api/teachers")]
    public class GiangViensController : BaseApiController
    {
        private readonly AppSettings appSettings;
        public GiangViensController(ddsvContext context, IOptions<AppSettings> appSettings) : base(context)
        {
            this.appSettings = appSettings.Value;
        }

        // GET: api/GiangViens
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GiangVien>>> GetList()
        {
            return await db.GiangVien.ToListAsync();
        }

        // GET: api/GiangViens/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GiangVien>> GetById(string id)
        {
            var giangVien = await db.GiangVien.FindAsync(id);

            if (giangVien == null)
            {
                return NotFound();
            }

            return giangVien;
        }

        // PUT: api/GiangViens/5
        [Route("update-teacher")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromQuery] string id, UpdateGiangVienBindingModel giangVien)
        {
            var t = db.GiangVien.FirstOrDefault(a => a.MaGv == id);

            if(t == null)
            {
                return Error(HttpStatusCode.NotFound, "Không Tìm Thấy Tài Khoản!");
            }
            try
            {
                if (!string.IsNullOrEmpty(giangVien.Matkhau))
                {
                    giangVien.Matkhau = PasswordHelpers.CreatePassword(giangVien.Matkhau);
                    db.Entry(t).Property(a => a.Matkhau).CurrentValue = giangVien.Matkhau;
                }

                db.Entry(t).Property(a => a.Hoten).CurrentValue = giangVien.Hoten;
                db.Entry(t).Property(a => a.Diachi).CurrentValue = giangVien.Diachi;
                db.Entry(t).Property(a => a.Sdt).CurrentValue = giangVien.SDT;
                db.Entry(t).Property(a => a.Ngaysinh).CurrentValue = giangVien.Ngaysinh;
                db.Entry(t).Property(a => a.Ngayvaolam).CurrentValue = giangVien.Ngayvaolam;
                db.Entry(t).State = EntityState.Modified;
                db.SaveChanges();
                return PostSuccess("Cập Nhật Tài Khoản Thành Công");
            }
            catch(Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // POST: api/GiangViens
        [HttpPost]
        [Route("create-teacher")]
        public async Task<IActionResult> Create(GiangVien teacher)
        {
            if (teacher == null)
            {
                return Error(HttpStatusCode.BadRequest, "Vui Lòng Điền Đầy Đủ Thông Tin!");
            }
            teacher.MaGv = RandomIDHelpers.RandomId().ToString();
            teacher.Matkhau = teacher.MaGv;
            teacher.Matkhau = PasswordHelpers.CreatePassword(teacher.MaGv);
            teacher.Maquyen = 2;
            db.GiangVien.Add(teacher);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (GiangVienExists(teacher.MaGv))
                {
                    return Error(HttpStatusCode.BadRequest, "Người Dùng Đã Tồn Tại!");
                }
                else
                {
                    throw;
                }
            }
            return PostSuccess("Đăng Kí Thành Công!");
        }

        [Route("teacher-login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] GiangVienDTO t)
        {
            if (string.IsNullOrEmpty(t.username) || string.IsNullOrEmpty(t.password))
            {
                return Error(HttpStatusCode.BadRequest, "Vui Lòng Điền Đầy Đủ Tên Đăng Nhập Và Mật Khẩu!");
            }

            var user = await db.GiangVien.FindAsync(t.username);
            if (user == null)
            {
                return Error(HttpStatusCode.NotFound, "Tài Khoản Không Tồn Tại!");
            }
            if (!BCrypt.Net.BCrypt.Verify(t.password, user.Matkhau))
            {
                return Error(HttpStatusCode.Conflict, "Sai Mật Khẩu. Vui Lòng Nhập Lại!");
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.MaGv),
                    new Claim(ClaimTypes.Role, user.Maquyen.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return Success(new
            {
                MaGV = user.MaGv,
                Hoten = user.Hoten,
                Diachi = user.Diachi,
                token = tokenString
            });

        }

        // DELETE: api/GiangViens/5
        [Route("delete-teacher")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<GiangVien>> DeleteGiangVien([FromQuery]string id)
        {
            var giangVien = await db.GiangVien.FindAsync(id);
            if (giangVien == null)
            {
                return NotFound();
            }

            db.GiangVien.Remove(giangVien);
            await db.SaveChangesAsync();

            return giangVien;
        }

        private bool GiangVienExists(string id)
        {
            return db.GiangVien.Any(e => e.MaGv == id);
        }
    }
}
