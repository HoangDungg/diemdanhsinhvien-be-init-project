﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DiemDanhSinhVien_BE.Model;
using System.Net;
using DiemDanhSinhVien_BE.Helpers;

namespace DiemDanhSinhVien_BE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SinhViensController : BaseApiController
    {

        public SinhViensController(ddsvContext context) : base(context)
        {
        }

        // GET: api/SinhViens
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SinhVien>>> GetList()
        {
            return await db.SinhVien.ToListAsync();
        }

        // GET: api/SinhViens/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SinhVien>> GetSinhVien(string id)
        {
            var sinhVien = await db.SinhVien.FindAsync(id);

            if (sinhVien == null)
            {
                return NotFound();
            }

            return sinhVien;
        }

        // PUT: api/SinhViens/5
        [Route("update-student")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromQuery]string id, SinhVien sinhVien)
        {
            if (id != sinhVien.MaSv)
            {
                return BadRequest();
            }

            db.Entry(sinhVien).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SinhVienExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SinhViens
        [Route("create-student")]
        [HttpPost]
        public async Task<IActionResult> PostSinhVien(SinhVien sv)
        {
            if(sv == null)
            {
                return Error(HttpStatusCode.BadRequest, "Vui Lòng Điền Đầy Đủ Thông Tin");
            }

            try
            {
                sv.MaSv = RandomIDHelpers.RandomId().ToString();
                sv.Matkhau = sv.MaSv;
                sv.Matkhau = PasswordHelpers.CreatePassword(sv.Matkhau);
                sv.Maquyen = 3;
                db.SinhVien.Add(sv);
                await db.SaveChangesAsync();
                return PostSuccess("Đăng Kí Thành Công!");
            }
            catch(Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, ex.Message);
            }
            
            
        }

        // DELETE: api/SinhViens/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<SinhVien>> DeleteSinhVien(string id)
        {
            var sinhVien = await db.SinhVien.FindAsync(id);
            if (sinhVien == null)
            {
                return NotFound();
            }

            db.SinhVien.Remove(sinhVien);
            await db.SaveChangesAsync();

            return sinhVien;
        }

        private bool SinhVienExists(string id)
        {
            return db.SinhVien.Any(e => e.MaSv == id);
        }
    }
}
