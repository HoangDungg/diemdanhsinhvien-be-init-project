﻿using DiemDanhSinhVien_BE.DTO;
using DiemDanhSinhVien_BE.Helpers;
using DiemDanhSinhVien_BE.Model;
using DiemDanhSinhVien_BE.Model.Binding.Admin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DiemDanhSinhVien_BE.Controllers
{
    
    [Route("api/admins")]
    public class AdminsController : BaseApiController
    {
        private readonly AppSettings appSettings;
        private IHttpContextAccessor httpContextAccessor;
        public AdminsController(ddsvContext context, IOptions<AppSettings> appSettings, IHttpContextAccessor httpContextAccessor) : base(context)
        {
            this.appSettings = appSettings.Value;
            this.httpContextAccessor = httpContextAccessor;
        }

        // GET: api/Admins
       
        [Route("list-admin")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Admin>>> GetList()
        {
            return await db.Admin.ToListAsync();
        }

        // GET: api/Admins/5
        
        [Route("get-admin")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAdmin(string id)
        {
            var admin = await db.Admin.FindAsync(id);

            if (admin == null)
            {
                return Error(HttpStatusCode.NotFound, "Người Dùng Không Tồn Tại!");
            }

            return Success(admin);
        }

        // PUT: api/Admins/5
        [Route("update-admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromQuery]string id, UpdateAdminBindingModel admin)
        {
            Admin user = db.Admin.FirstOrDefault(a => a.MaAdmin == id);

            if(user == null)
            {
                return Error(HttpStatusCode.NotFound, "Không Tìm Thấy Tài Khoản. Vui Lòng Kiểm Tra Lại Tên Đăng Nhập");
            }

            try
            {
                if (!string.IsNullOrEmpty(admin.Matkhau))
                {
                    admin.Matkhau = PasswordHelpers.CreatePassword(admin.Matkhau);
                    db.Entry(user).Property(a => a.Matkhau).CurrentValue = admin.Matkhau;
                }

                db.Entry(user).Property(a => a.Hoten).CurrentValue = admin.Hoten;
                db.Entry(user).Property(a => a.Diachi).CurrentValue = admin.Diachi;
                db.Entry(user).Property(a => a.Sdt).CurrentValue = admin.SDT;
                db.Entry(user).Property(a => a.Gioitinh).CurrentValue = admin.Gioitinh;
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Success(new
                {
                    Hoten = user.Hoten,
                    Diachi = user.Diachi,
                    Sdt = user.Sdt,
                    Gioitinh = user.Gioitinh
                });
            }
            catch(Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // POST: api/Admins
        [Authorize(Roles = TenQuyen.Admin)]
        [Route("register-admin")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Create(Admin admin)
        {
            Log.Information("Log: Creating admmin");

            if (admin == null)
            {
                return Error(HttpStatusCode.BadRequest, "Vui Lòng Điền Đầy Đủ Thông Tin");
            }
            admin.MaAdmin = RandomIDHelpers.RandomId().ToString();
            admin.Matkhau = admin.MaAdmin;
            admin.Maquyen = int.Parse(TenQuyen.Admin);
            admin.Matkhau = PasswordHelpers.CreatePassword(admin.Matkhau);
            db.Admin.Add(admin);
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AdminExists(admin.MaAdmin))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return PostSuccess("Đăng Kí Thành Công");
        }

        //POST: Login Admin
        [Route("login-admin")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]AdminDTO admin)
        {
            if (string.IsNullOrEmpty(admin.username) || string.IsNullOrEmpty(admin.password))
            {
                return Error(HttpStatusCode.BadRequest, "Vui lòng điền đầy đủ tên đăng nhập và mật khẩu");
            }

            var user = await db.Admin.FindAsync(admin.username);

            if (user == null)
            {
                return Error(HttpStatusCode.NotFound, "Tên đăng nhập hoặc mật khẩu không đúng");
            }

            if (!BCrypt.Net.BCrypt.Verify(admin.password, user.Matkhau))
            {
                return Error(HttpStatusCode.BadRequest, "Sai Mật Khẩu");
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.MaAdmin),
                    new Claim(ClaimTypes.Role, user.Maquyen.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return Success(new
            {
                MaAdmin = user.MaAdmin,
                Hoten = user.Hoten,
                Diachi = user.Diachi,
                token = tokenString
            });
        }

        // DELETE: api/Admins/5
        [Route("delete-admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAdmin([FromQuery]string id)
        {
            var admin = await db.Admin.FindAsync(id);
            if (admin == null)
            {
                return Error(HttpStatusCode.NotFound, "Không Tìm Thấy Thông Tin Người Dùng!");
            }

            db.Admin.Remove(admin);
            await db.SaveChangesAsync();

            return PostSuccess("Xoá Thành Công!");
        }

        private bool AdminExists(string id)
        {
            return db.Admin.Any(e => e.MaAdmin == id);
        }

    }
}
