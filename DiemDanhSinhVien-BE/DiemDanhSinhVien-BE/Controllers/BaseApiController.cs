﻿
using DiemDanhSinhVien_BE.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DiemDanhSinhVien_BE.Controllers
{
    [ApiController]
    public class BaseApiController : ControllerBase
    {
        protected readonly ddsvContext db;
        public BaseApiController(ddsvContext db)
        {
            this.db = db;
        }

        protected IActionResult Success<T>(T dataResp)
        {
            return Ok(new { 
                status = "success",
                statusCode = 200,
                data = dataResp
            });
        }

        protected IActionResult PostSuccess (string msg = "")
        {
            return Ok(new { 
                status = "success",
                statusCode = 200,
                message = msg
            });
        }

        protected IActionResult Error(HttpStatusCode sttCode, string msg = "")
        {
            var errResponse = new
            {
                status = "failed",
                statusCode = sttCode,
                message = msg,
            };

            switch (sttCode)
            {
                case HttpStatusCode.NotFound:
                    return NotFound(errResponse);
                default:
                    return BadRequest(errResponse);
            }
        }
    }
}
