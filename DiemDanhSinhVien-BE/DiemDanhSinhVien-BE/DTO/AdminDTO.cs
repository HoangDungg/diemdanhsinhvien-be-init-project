﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiemDanhSinhVien_BE.DTO
{
    public class AdminDTO
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
