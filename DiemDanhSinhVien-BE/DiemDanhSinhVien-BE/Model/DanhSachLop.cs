﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class DanhSachLop
    {
        public string MaSv { get; set; }
        public string MaLmh { get; set; }
        public int? Soluong { get; set; }
        public string MaDsl { get; set; }
        public string MaLopSv { get; set; }
        public string MaMh { get; set; }
        public string MaGv { get; set; }

        public virtual LopMonHoc Ma { get; set; }
    }
}
