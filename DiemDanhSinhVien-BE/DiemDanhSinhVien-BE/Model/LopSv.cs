﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class LopSv
    {
        public int Id { get; set; }
        public string MaLopSv { get; set; }
        public string TenLop { get; set; }
    }
}
