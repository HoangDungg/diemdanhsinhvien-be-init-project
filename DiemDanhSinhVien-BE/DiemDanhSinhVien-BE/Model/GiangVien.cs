﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class GiangVien
    {
        public GiangVien()
        {
            LopMonHoc = new HashSet<LopMonHoc>();
        }

        public int Id { get; set; }
        public int Maquyen { get; set; }
        public string MaGv { get; set; }
        public string Hoten { get; set; }
        public string Matkhau { get; set; }
        public string Ngaysinh { get; set; }
        public string Ngayvaolam { get; set; }
        public int? Sdt { get; set; }
        public string Diachi { get; set; }
        public string Gioitinh { get; set; }
        public string Email { get; set; }

        public virtual Quyen MaquyenNavigation { get; set; }
        public virtual ICollection<LopMonHoc> LopMonHoc { get; set; }
    }
}
