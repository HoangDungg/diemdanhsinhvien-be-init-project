﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class LopMonHoc
    {
        public LopMonHoc()
        {
            DanhSachLop = new HashSet<DanhSachLop>();
        }

        public int Id { get; set; }
        public string MaGv { get; set; }
        public string MaMh { get; set; }
        public string MaLmh { get; set; }
        public string TenLmh { get; set; }
        public string Phong { get; set; }
        public DateTime? Thoigian { get; set; }
        public int? Sotiet { get; set; }
        public int? Sobuoi { get; set; }
        public string Nhom { get; set; }

        public virtual GiangVien MaGvNavigation { get; set; }
        public virtual Monhoc MaMhNavigation { get; set; }
        public virtual ICollection<DanhSachLop> DanhSachLop { get; set; }
    }
}
