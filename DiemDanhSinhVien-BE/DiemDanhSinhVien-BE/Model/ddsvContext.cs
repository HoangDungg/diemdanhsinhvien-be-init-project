﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class ddsvContext : DbContext
    {
        public ddsvContext()
        {
        }

        public ddsvContext(DbContextOptions<ddsvContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Admin> Admin { get; set; }
        public virtual DbSet<ChiTietQuyen> ChiTietQuyen { get; set; }
        public virtual DbSet<DanhSachLop> DanhSachLop { get; set; }
        public virtual DbSet<DiemDanh> DiemDanh { get; set; }
        public virtual DbSet<GiangVien> GiangVien { get; set; }
        public virtual DbSet<Khoa> Khoa { get; set; }
        public virtual DbSet<LopMonHoc> LopMonHoc { get; set; }
        public virtual DbSet<LopSv> LopSv { get; set; }
        public virtual DbSet<Monhoc> Monhoc { get; set; }
        public virtual DbSet<Quyen> Quyen { get; set; }
        public virtual DbSet<QuyenChucNang> QuyenChucNang { get; set; }
        public virtual DbSet<SinhVien> SinhVien { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer("Server=tcp:attendancestudent.database.windows.net,1433;Initial Catalog=AttendancesStudent;Persist Security Info=False;User ID=teamadmin;Password=Admin123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Admin>(entity =>
            {
                entity.HasKey(e => e.MaAdmin)
                    .HasName("PK__Admin__49341E38C9A9A784");

                entity.Property(e => e.MaAdmin)
                    .HasMaxLength(100)
                    .ValueGeneratedNever();

                entity.Property(e => e.Diachi).HasMaxLength(1000);

                entity.Property(e => e.Email).HasMaxLength(1000);

                entity.Property(e => e.Gioitinh)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Hoten).HasMaxLength(1000);

                entity.Property(e => e.Id)
                            .ValueGeneratedOnAdd()
                            .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;

                entity.Property(e => e.Matkhau).HasMaxLength(1000);

                entity.Property(e => e.Ngaysinh).HasMaxLength(200);

                entity.Property(e => e.Sdt).HasColumnName("SDT");

                entity.HasOne(d => d.MaquyenNavigation)
                    .WithMany(p => p.Admin)
                    .HasForeignKey(d => d.Maquyen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Admin__Maquyen__592635D8");
            });

            modelBuilder.Entity<ChiTietQuyen>(entity =>
            {
                entity.HasKey(e => new { e.Maquyen, e.MaQcn })
                    .HasName("PK__ChiTietQ__66E1141B238766E3");

                entity.Property(e => e.MaQcn).HasColumnName("MaQCN");

                entity.HasOne(d => d.MaQcnNavigation)
                    .WithMany(p => p.ChiTietQuyen)
                    .HasForeignKey(d => d.MaQcn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ChiTietQu__MaQCN__5FD33367");

                entity.HasOne(d => d.MaquyenNavigation)
                    .WithMany(p => p.ChiTietQuyen)
                    .HasForeignKey(d => d.Maquyen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ChiTietQu__Maquy__5832119F");
            });

            modelBuilder.Entity<DanhSachLop>(entity =>
            {
                entity.HasKey(e => new { e.MaSv, e.MaLmh, e.MaDsl, e.MaLopSv, e.MaMh, e.MaGv })
                    .HasName("PK__DanhSach__249F4E4C7428F1BB");

                entity.Property(e => e.MaSv)
                    .HasColumnName("MaSV")
                    .HasMaxLength(20);

                entity.Property(e => e.MaLmh)
                    .HasColumnName("MaLMH")
                    .HasMaxLength(1000);

                entity.Property(e => e.MaDsl)
                    .HasColumnName("MaDSL")
                    .HasMaxLength(1000);

                entity.Property(e => e.MaLopSv)
                    .HasColumnName("MaLopSV")
                    .HasMaxLength(100);

                entity.Property(e => e.MaMh)
                    .HasColumnName("MaMH")
                    .HasMaxLength(1000);

                entity.Property(e => e.MaGv)
                    .HasColumnName("MaGV")
                    .HasMaxLength(20);

                entity.HasOne(d => d.Ma)
                    .WithMany(p => p.DanhSachLop)
                    .HasForeignKey(d => new { d.MaGv, d.MaMh, d.MaLmh })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DanhSachLop__5DEAEAF5");
            });

            modelBuilder.Entity<DiemDanh>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.MaSv, e.MaLmh, e.MaDsl, e.MaLopSv, e.MaMh, e.MaGv })
                    .HasName("PK__DiemDanh__E05D18E3A7523B9A");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.MaSv)
                    .HasColumnName("MaSV")
                    .HasMaxLength(20);

                entity.Property(e => e.MaLmh)
                    .HasColumnName("MaLMH")
                    .HasMaxLength(1000);

                entity.Property(e => e.MaDsl)
                    .HasColumnName("MaDSL")
                    .HasMaxLength(1000);

                entity.Property(e => e.MaLopSv)
                    .HasColumnName("MaLopSV")
                    .HasMaxLength(20);

                entity.Property(e => e.MaMh)
                    .HasColumnName("MaMH")
                    .HasMaxLength(1000);

                entity.Property(e => e.MaGv)
                    .HasColumnName("MaGV")
                    .HasMaxLength(20);

                entity.Property(e => e.Thoigian).HasColumnType("datetime");
            });

            modelBuilder.Entity<GiangVien>(entity =>
            {
                entity.HasKey(e => e.MaGv)
                    .HasName("PK__GiangVie__2725AEF34FFDE70D");

                entity.Property(e => e.MaGv)
                    .HasColumnName("MaGV")
                    .HasMaxLength(20)
                    .ValueGeneratedNever();

                entity.Property(e => e.Diachi).HasMaxLength(1000);

                entity.Property(e => e.Email).HasMaxLength(1000);

                entity.Property(e => e.Gioitinh).HasMaxLength(10);

                entity.Property(e => e.Hoten).HasMaxLength(1000);

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd()
                      .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;

                entity.Property(e => e.Matkhau).HasMaxLength(1000);

                entity.Property(e => e.Ngaysinh).HasMaxLength(200);

                entity.Property(e => e.Ngayvaolam).HasMaxLength(200);

                entity.Property(e => e.Sdt).HasColumnName("SDT");

                entity.HasOne(d => d.MaquyenNavigation)
                    .WithMany(p => p.GiangVien)
                    .HasForeignKey(d => d.Maquyen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__GiangVien__Maquy__5A1A5A11");
            });

            modelBuilder.Entity<Khoa>(entity =>
            {
                entity.HasKey(e => e.MaKhoa)
                    .HasName("PK__Khoa__65390405151D562F");

                entity.Property(e => e.MaKhoa)
                    .HasMaxLength(20)
                    .ValueGeneratedNever();

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd()
                      .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;

                entity.Property(e => e.TenKhoa).HasMaxLength(20);
            });

            modelBuilder.Entity<LopMonHoc>(entity =>
            {
                entity.HasKey(e => new { e.MaGv, e.MaMh, e.MaLmh })
                    .HasName("PK__LopMonHo__156C68C15281FFDD");

                entity.Property(e => e.MaGv)
                    .HasColumnName("MaGV")
                    .HasMaxLength(20);

                entity.Property(e => e.MaMh)
                    .HasColumnName("MaMH")
                    .HasMaxLength(1000);

                entity.Property(e => e.MaLmh)
                    .HasColumnName("MaLMH")
                    .HasMaxLength(1000);

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd()
                      .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;

                entity.Property(e => e.Nhom).HasMaxLength(100);

                entity.Property(e => e.Phong)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TenLmh)
                    .HasColumnName("TenLMH")
                    .HasMaxLength(1000);

                entity.Property(e => e.Thoigian).HasColumnType("datetime");

                entity.HasOne(d => d.MaGvNavigation)
                    .WithMany(p => p.LopMonHoc)
                    .HasForeignKey(d => d.MaGv)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LopMonHoc__MaGV__5649C92D");

                entity.HasOne(d => d.MaMhNavigation)
                    .WithMany(p => p.LopMonHoc)
                    .HasForeignKey(d => d.MaMh)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LopMonHoc__MaMH__5C02A283");
            });

            modelBuilder.Entity<LopSv>(entity =>
            {
                entity.HasKey(e => e.MaLopSv)
                    .HasName("PK__LopSV__976A85114F26C7A0");

                entity.ToTable("LopSV");

                entity.Property(e => e.MaLopSv)
                    .HasColumnName("MaLopSV")
                    .HasMaxLength(100)
                    .ValueGeneratedNever();

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd()
                      .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;

                entity.Property(e => e.TenLop).HasMaxLength(200);
            });

            modelBuilder.Entity<Monhoc>(entity =>
            {
                entity.HasKey(e => e.MaMh)
                    .HasName("PK__Monhoc__2725DFD9E6223E62");

                entity.Property(e => e.MaMh)
                    .HasColumnName("MaMH")
                    .HasMaxLength(1000)
                    .ValueGeneratedNever();

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd()
                      .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;

                entity.Property(e => e.MaKhoa)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.TenMh)
                    .HasColumnName("TenMH")
                    .HasMaxLength(1000);
            });

            modelBuilder.Entity<Quyen>(entity =>
            {
                entity.HasKey(e => e.Maquyen)
                    .HasName("PK__Quyen__C577E6E542239F3C");

                entity.Property(e => e.Maquyen).ValueGeneratedNever();

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd()
                      .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;

                entity.Property(e => e.Tenquyen).HasMaxLength(10);
            });

            modelBuilder.Entity<QuyenChucNang>(entity =>
            {
                entity.HasKey(e => e.MaQcn)
                    .HasName("PK__QuyenChu__396F2FEB755A3168");

                entity.Property(e => e.MaQcn)
                    .HasColumnName("MaQCN")
                    .ValueGeneratedNever();

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd()
                      .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;

                entity.Property(e => e.Noidung).HasMaxLength(1000);
            });

            modelBuilder.Entity<SinhVien>(entity =>
            {
                entity.HasKey(e => new { e.MaSv, e.MaLopSv })
                    .HasName("PK__SinhVien__2E53A04B402EB0E5");

                entity.Property(e => e.MaSv)
                    .HasColumnName("MaSV")
                    .HasMaxLength(20);

                entity.Property(e => e.MaLopSv)
                    .HasColumnName("MaLopSV")
                    .HasMaxLength(20);

                entity.Property(e => e.Diachi).HasMaxLength(1000);

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.Hoten).HasMaxLength(1000);

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd()
                      .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;

                entity.Property(e => e.Matkhau).HasMaxLength(1000);

                entity.Property(e => e.Ngaynhaphoc).HasMaxLength(100);

                entity.Property(e => e.Ngaysinh).HasMaxLength(100);

                entity.Property(e => e.Sdt).HasColumnName("SDT");

                entity.Property(e => e.Sdtcha).HasColumnName("SDTCha");

                entity.Property(e => e.Sdtme).HasColumnName("SDTMe");

                entity.HasOne(d => d.MaquyenNavigation)
                    .WithMany(p => p.SinhVien)
                    .HasForeignKey(d => d.Maquyen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SinhVien__Maquye__5B0E7E4A");
            });
        }
    }
}
