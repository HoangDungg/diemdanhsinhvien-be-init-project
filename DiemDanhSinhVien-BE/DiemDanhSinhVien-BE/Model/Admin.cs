﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class Admin
    {
        public int Id { get; set; }
        public int Maquyen { get; set; }
        public string MaAdmin { get; set; }
        public string Hoten { get; set; }
        public string Matkhau { get; set; }
        public string Ngaysinh { get; set; }
        public int? Sdt { get; set; }
        public string Diachi { get; set; }
        public string Gioitinh { get; set; }
        public string Email { get; set; }

        public virtual Quyen MaquyenNavigation { get; set; }
    }
}
