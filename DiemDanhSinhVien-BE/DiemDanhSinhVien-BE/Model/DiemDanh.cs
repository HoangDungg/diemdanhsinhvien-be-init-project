﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class DiemDanh
    {
        public int Id { get; set; }
        public string MaSv { get; set; }
        public string MaLmh { get; set; }
        public string MaDsl { get; set; }
        public DateTime? Thoigian { get; set; }
        public int? MaRandom { get; set; }
        public int? Sobuoi { get; set; }
        public int? Sotiet { get; set; }
        public string MaLopSv { get; set; }
        public string MaMh { get; set; }
        public string MaGv { get; set; }
        public bool? TrangThai { get; set; }
    }
}
