﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class Monhoc
    {
        public Monhoc()
        {
            LopMonHoc = new HashSet<LopMonHoc>();
        }

        public int Id { get; set; }
        public string MaKhoa { get; set; }
        public string MaMh { get; set; }
        public string TenMh { get; set; }

        public virtual ICollection<LopMonHoc> LopMonHoc { get; set; }
    }
}
