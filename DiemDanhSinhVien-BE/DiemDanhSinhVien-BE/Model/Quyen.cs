﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class Quyen
    {
        public Quyen()
        {
            Admin = new HashSet<Admin>();
            ChiTietQuyen = new HashSet<ChiTietQuyen>();
            GiangVien = new HashSet<GiangVien>();
            SinhVien = new HashSet<SinhVien>();
        }

        public int Id { get; set; }
        public string Tenquyen { get; set; }
        public int Maquyen { get; set; }

        public virtual ICollection<Admin> Admin { get; set; }
        public virtual ICollection<ChiTietQuyen> ChiTietQuyen { get; set; }
        public virtual ICollection<GiangVien> GiangVien { get; set; }
        public virtual ICollection<SinhVien> SinhVien { get; set; }
    }
}
