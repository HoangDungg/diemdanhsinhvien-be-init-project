﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiemDanhSinhVien_BE.Model
{
    public class TenQuyen
    {
        public const string Admin = "1";
        public const string Teacher = "2";
        public const string Student = "3";
    }
}
