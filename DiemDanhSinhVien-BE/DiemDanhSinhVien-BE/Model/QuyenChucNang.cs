﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class QuyenChucNang
    {
        public QuyenChucNang()
        {
            ChiTietQuyen = new HashSet<ChiTietQuyen>();
        }

        public int Id { get; set; }
        public int MaQcn { get; set; }
        public string Noidung { get; set; }

        public virtual ICollection<ChiTietQuyen> ChiTietQuyen { get; set; }
    }
}
