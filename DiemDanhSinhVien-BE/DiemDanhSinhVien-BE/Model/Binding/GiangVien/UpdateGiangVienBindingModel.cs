﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DiemDanhSinhVien_BE.Model.Binding.GiangVien
{
    public class UpdateGiangVienBindingModel
    {

        [Display(Name = "Họ Tên")]
        public string Hoten { get; set; }
        [Display(Name = "Địa Chỉ")]
        public string Diachi { get; set; }
        [Display(Name = "Mật Khẩu")]
        [DataType(DataType.Password)]
        public string Matkhau { get; set; }
        [Display(Name = "Giới Tính")]
        public string Gioitinh { get; set; }
        [Display(Name = "Số Điện Thoại")]
        public int? SDT { get; set; }
        [Display(Name = "Ngày Sinh")]
        public string Ngaysinh { get; set; }
        [Display(Name = "Ngày Vào Làm")]
        public string Ngayvaolam { get; set; }
    }
}
