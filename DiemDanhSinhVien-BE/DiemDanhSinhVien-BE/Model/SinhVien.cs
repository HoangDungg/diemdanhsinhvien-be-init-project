﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class SinhVien
    {
        public int Id { get; set; }
        public int Maquyen { get; set; }
        public string MaSv { get; set; }
        public string Hoten { get; set; }
        public string Matkhau { get; set; }
        public string Ngaysinh { get; set; }
        public string Ngaynhaphoc { get; set; }
        public int? Sdt { get; set; }
        public int? Sdtme { get; set; }
        public int? Sdtcha { get; set; }
        public string Diachi { get; set; }
        public string MaLopSv { get; set; }
        public string Email { get; set; }

        public virtual Quyen MaquyenNavigation { get; set; }
    }
}
