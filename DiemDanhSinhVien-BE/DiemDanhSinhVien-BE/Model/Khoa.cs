﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class Khoa
    {
        public int Id { get; set; }
        public string MaKhoa { get; set; }
        public string TenKhoa { get; set; }
    }
}
