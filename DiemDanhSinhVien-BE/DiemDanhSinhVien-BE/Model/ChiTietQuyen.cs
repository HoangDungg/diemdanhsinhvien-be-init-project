﻿using System;
using System.Collections.Generic;

namespace DiemDanhSinhVien_BE.Model
{
    public partial class ChiTietQuyen
    {
        public int Maquyen { get; set; }
        public int MaQcn { get; set; }
        public bool? Trangthai { get; set; }

        public virtual QuyenChucNang MaQcnNavigation { get; set; }
        public virtual Quyen MaquyenNavigation { get; set; }
    }
}
