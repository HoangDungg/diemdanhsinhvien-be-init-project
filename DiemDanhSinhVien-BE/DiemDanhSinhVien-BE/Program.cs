﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace DiemDanhSinhVien_BE
{
    public class Program
    {
        //public static async Task Main(string[] args)
        //{
        //    //    Log.Logger = new LoggerConfiguration()
        //    //        .MinimumLevel.Debug()
        //    //        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
        //    //        .Enrich.FromLogContext()
        //    //        .WriteTo.Console()
        //    //        .WriteTo.File("logs\\DDSVLogs.txt", rollingInterval: RollingInterval.Day)
        //    //        .CreateLogger();
        //    //    try
        //    //    {
        //    //        Log.Information("Starting web host");
        //    //        var host = CreateWebHostBuilder(args);
        //    //        host.Build();
        //    //    }
        //    //    catch (Exception ex)
        //    //    {
        //    //        Log.Fatal(ex, "Host terminated unexpectedly");
        //    //    }
        //    //    finally
        //    //    {
        //    //        Log.CloseAndFlush();


        //    //    }
        //    //}
        //    //public static IHostBuilder CreateWebHostBuilder(string[] args) =>
        //    //    //WebHost.CreateDefaultBuilder()
        //    //    //.UseStartup<Startup>()
        //    //    //.UseSerilog()
        //    //    //.Build();
        //    //    new HostBuilder()
        //    //    .UseSerilog();



        //}
        public static void  Main(string[] args)
        {
            Log.Information("Starting web host");
            var host = CreateWebHostBuilder(args);
            host.Run();

        }

        public static IWebHost CreateWebHostBuilder(string[] args)
        {
            var builtConfig = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.File("logs\\DDSVLogs.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            try
            {
                return WebHost.CreateDefaultBuilder(args)

                    .ConfigureAppConfiguration((hostingContext, config) =>
                    {
                        config.AddConfiguration(builtConfig);
                    })
                    .ConfigureLogging(logging =>
                    {
                        logging.AddSerilog();
                    })
                    .UseStartup<Startup>()
                    .Build();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host builder error");

                throw;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}
